import {MENU_ACTIONS} from '../actions/constants';

const menuReducer = (state = {selectedMenu: 0, selectedSubMenu: 0}, action) => {
  switch (action.type) {
  case MENU_ACTIONS.GET_MENU:
    return {
      ...state,
      data: action.data,
    };
  case MENU_ACTIONS.CHANGE_MENU:
    return {
      ...state,
      selectedMenu: action.data.selectedMenu,
      selectedSubMenu: action.data.selectedSubMenu,
    };
  default:
    return {...state};
  }
};

export default menuReducer;
