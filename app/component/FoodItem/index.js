import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import styles from './styles';

class FoodItem extends Component {
  render() {
    const {foodProps} = this.props;
    return (
      <View style={styles.foodParentView}>
        <View style={styles.foodDetailsView}>
          <Text style={styles.foodName}>{foodProps.name}</Text>
          <Text>${Math.round(foodProps.price * 100, 10) / 100}</Text>
        </View>
        <Image
          style={styles.foodImg}
          source={{
            uri: `https://via.placeholder.com/300.png?text=${foodProps.name}`,
          }}
        />
      </View>
    );
  }
}

export default FoodItem;
