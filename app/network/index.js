const invokeApi = async (dispatch, url, request) => {
  const onSuccess = async (response) => {
    console.log('Success call back', response);
    return response;
  };
  const onError = async (error) => {
    console.log('Erro call back', error);
  };

  return fetch(url)
    .then((response) => {
      console.log('First promise', response);
      return response.text();
    })
    .then((responseText) => {
      const responseJSON = JSON.parse(responseText);
      return responseJSON;
    })
    .then((responseJSON) => {
      console.log('Third promise', responseJSON);
      return onSuccess(responseJSON);
    })
    .catch((error) => onError(error));
};

export default invokeApi;
