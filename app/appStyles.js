import { StyleSheet } from 'react-native';

const appStyles = StyleSheet.create({
  parentView: { flex: 1 },
});

export default appStyles;
