import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  foodContainer: {
    padding: 10,
    margin: 10,
    flexDirection: 'row',
  },
  foodItemsParent: {
    flex: 0.7,
  },
});

export default styles;
