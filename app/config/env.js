const config = {
  colors: {
    bgColor: '#ffffff',
    statusBarBgColor: '#061a9b',
    menuBgColor: '#381459',
    subMenuBGColor: '#2b1c53',
    menuTxtColorSelected: '#ffffff',
    menuTxtColorUnSelected: '#cbcccb',
  },
  apiEndPoints: {
    basURL: 'http://localhost:3001/',
    menus: {
      url: 'api/menu',
      httpMethod: 'GET',
    },
  },
};

export default config;
