import {MENU_ACTIONS} from './constants';

export function getMenuDetails(menuData) {
  return {
    type: MENU_ACTIONS.GET_MENU,
    data: menuData,
  };
}

export function menuChange(selectedMenu) {
  return {
    type: MENU_ACTIONS.CHANGE_MENU,
    data: selectedMenu,
  };
}
