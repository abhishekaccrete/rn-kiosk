import invokeApi from '../network';
import {getMenuDetails} from '../redux/actions/menuActions';
import config from '../config/env';

export const getMenuAPI = (request) => (dispatch) => {
  const {apiEndPoints} = config;
  const sMenuApiUrl = apiEndPoints.basURL + apiEndPoints.menus.url;
  invokeApi(dispatch, sMenuApiUrl, request)
    .then((menuResponse) => dispatch(getMenuDetails(menuResponse)))
    .catch((menuError) => console.log('getMenuAPI FAILURE', menuError));
};
