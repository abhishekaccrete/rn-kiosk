export const MENU_ACTIONS = {
  GET_MENU: 'getMenu',
  CHANGE_MENU: 'menuChanged',
};
