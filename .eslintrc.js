module.exports = {
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true
    }
  },
  env: {
    'react-native/react-native': true,
    browser: true,
    es6: true,
    node: true,
    jest: true
  },
  plugins: ['react', 'react-native', 'prettier'],
  extends: ['airbnb', 'prettier', 'prettier/react', 'prettier/standard'],
  rules: {
    'import/no-cycle': [2, { maxDepth: 1 }],
    'no-underscore-dangle': 2,
    indent: ['error', 2],
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'jsx-a11y/anchor-is-valid': [
      'error',
      {
        components: ['Link']
      }
    ],
    'linebreak-style': 2,
    'no-use-before-define': 2,
    'react/prop-types': 2,
    'comma-dangle': 2,
    'react-native/no-unused-styles': 2,
    'react-native/split-platform-components': 2,
    'react-native/no-inline-styles': 2,
    'react-native/no-color-literals': 2,
    'react-native/no-raw-text': 2
  },
  overrides: [
    {
      files: ['app/**/**.test.js', '__tests__/**/**.js', '**.js'],
      rules: {
        'no-undef': 'off'
      }
    }
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    __DEV__: 'readonly'
  }
};
