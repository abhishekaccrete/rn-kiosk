import React, {Component} from 'react';
import {View, Text, FlatList} from 'react-native';
import appStyles from '../../appStyles';
import styles from './styles';
import Menu from '../../component/Menu';
import FoodItem from '../../component/FoodItem';

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.getMenuComponent = this.getMenuComponent.bind(this);
    this.getFoodPage = this.getFoodPage.bind(this);
    this.getMenuAndFoodPage = this.getMenuAndFoodPage.bind(this);
    this.showFoodItem = this.showFoodItem.bind(this);
  }

  componentDidMount() {
    const {getRestaurantMenu} = this.props;
    getRestaurantMenu();
  }

  getMenuComponent(data, selectedMenu, selectedSubMenu) {
    return (
      <Menu
        selectedMenu={selectedMenu}
        selectedSubMenu={selectedSubMenu}
        menuChanged={(index, forMenu) => this.menuChanged(index, forMenu)}
        menuData={data.MenuGroups}
      />
    );
  }

  showFoodItem({item}) {
    return <FoodItem foodProps={item} />;
  }

  getFoodPage(data, selectedMenu, selectedSubMenu) {
    const foodItems =
      data.MenuGroups[selectedMenu].categories[selectedSubMenu].items;
    return (
      <View style={styles.foodContainer}>
        <View style={styles.foodItemsParent}>
          <FlatList
            numColumns={2}
            data={foodItems}
            renderItem={(item) => this.showFoodItem(item)}
          />
        </View>
      </View>
    );
  }

  getMenuAndFoodPage(data, selectedMenu, selectedSubMenu) {
    return (
      <View>
        {this.getMenuComponent(data, selectedMenu, selectedSubMenu)}
        {this.getFoodPage(data, selectedMenu, selectedSubMenu)}
      </View>
    );
  }

  menuChanged(index, forMenu) {
    const {changeMenu, selectedMenu} = this.props;
    const menuChangeRequest = {
      selectedMenu: forMenu ? index : selectedMenu,
      selectedSubMenu: forMenu ? 0 : index,
    };
    changeMenu(menuChangeRequest);
  }

  render() {
    const {data, selectedMenu, selectedSubMenu} = this.props;
    console.log('data in render', data);
    return (
      <View style={appStyles.parentView}>
        {data
          ? this.getMenuAndFoodPage(data.data, selectedMenu, selectedSubMenu)
          : null}
      </View>
    );
  }
}

export default HomeScreen;
