import React, { Component } from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from "./styles";

const MenuItem = (props) => {
  const { menuItem, selected, onChange, menuIndex } = props;
  return (
    <TouchableOpacity onPress={() => onChange(menuIndex)} style={styles.menuItemParent}>
      <Text style={[styles.txtStyle, selected ? styles.txtStyleSelected: null]}>
        {menuItem.name}
      </Text>
    </TouchableOpacity>
  );
}

export default MenuItem;
