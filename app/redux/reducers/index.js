import menuReducer from './menuReducer';

// in case there are more than 1 reducers, we can always user combineReducers method

const appReducer = menuReducer;

export default appReducer;
