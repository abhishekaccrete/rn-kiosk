import {StyleSheet} from 'react-native';
import config from '../../config/env';

const styles = StyleSheet.create({
  subMenuBottom: {
    paddingBottom: 20,
  },
  menuParent: {
    backgroundColor: config.colors.menuBgColor,
  },
  subMenuParent: {
    backgroundColor: config.colors.subMenuBGColor,
  },
  menu: {
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    flexDirection: 'row',
  },
});

export default styles;
