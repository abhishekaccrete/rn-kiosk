import { StyleSheet} from 'react-native'
import config from "../../config/env";

const styles = StyleSheet.create({
  txtStyleSelected: {
    fontWeight: 'bold',
    color: config.colors.menuTxtColorSelected
  },
  menuItemParent: {
    paddingLeft: 20
  },
  txtStyle: {
    color: config.colors.menuTxtColorUnSelected,
    textAlign: 'center',
    fontSize: 20
  },
});



export default styles;