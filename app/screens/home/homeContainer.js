import {connect} from 'react-redux';
import HomeScreen from './homeComponent';
import {getMenuAPI} from '../../thunk/menu-thunk';
import {menuChange} from '../../redux/actions/menuActions';

function mapStateToProps(state) {
  return {
    ...state,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getRestaurantMenu: () => dispatch(getMenuAPI()),
    changeMenu: (menuChangeRequest) => dispatch(menuChange(menuChangeRequest)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
