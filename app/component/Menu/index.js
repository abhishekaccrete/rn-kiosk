import React, {Component} from 'react';
import {View, ScrollView, FlatList} from 'react-native';
import _ from 'lodash';
import styles from './styles';
import MenuItem from '../MenuItem';

class Menu extends Component {
  constructor(props) {
    super(props);
    this.drawMenu = this.drawMenu.bind(this);
    this.menuChange = this.menuChange.bind(this);
    this.getMenuItem = this.getMenuItem.bind(this);
  }

  getMenuItem(item, index, selectedItem, forMenu) {
    return (
      <MenuItem
        onChange={() => this.menuChange(index, forMenu)}
        menuIndex={index}
        menuItem={item}
        selected={index === selectedItem}
      />
    );
  }

  menuChange(index, forMenu) {
    const {menuChanged} = this.props;
    menuChanged(index, forMenu);
  }

  drawMenu(menuData, selectedItem, forMenu) {
    return (
      <FlatList
        contentContainerStyle={[
          styles.menu,
          forMenu ? null : styles.subMenuBottom,
        ]}
        horizontal
        data={menuData}
        renderItem={({item, index}) =>
          this.getMenuItem(item, index, selectedItem, forMenu)
        }
      />
    );
  }

  render() {
    const {menuData, selectedMenu, selectedSubMenu} = this.props;
    return (
      <View style={styles.menuParent}>
        <View style={styles.menu}>
          {this.drawMenu(menuData, selectedMenu, true)}
        </View>
        <View style={[styles.menu, styles.subMenuParent]}>
          {this.drawMenu(
            menuData[selectedMenu].categories,
            selectedSubMenu,
            false,
          )}
        </View>
      </View>
    );
  }
}

export default Menu;
