import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  foodName: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  foodParentView: {
    flexDirection: 'row',
    flex: 1,
    width: 400,
  },
  foodImg: {
    justifyContent: 'flex-end',
    width: 100,
    height: 100,
  },
  foodDetailsView: {
    flex: 1,
  },
});

export default styles;
